import {StyleSheet} from 'react-native';
import elevation from '../../theme/elevation';
export const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
  },
  heading: {
    alignItems: 'center',
    backgroundColor: 'blue',
    width: '100%',
    paddingVertical: 12,
    paddingHorizontal: 12,
  },
  title: {
    color: 'white',
    fontSize: 20,
    fontWeight: '600',
    letterSpacing: 2,
  },
  header: {
    width: '100%',
    // height: '30%',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  inputFields: {
    marginTop: 20,
    width: '80%',
    // borderWidth: 1,
    paddingHorizontal: 6,
    // borderRadius: 6,
    borderBottomWidth: 1,
  },
  servicesContainer: {
    marginTop: 36,
    flexDirection: 'row',
    height: 60,
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '80%',
  },
  servicesItem: {
    backgroundColor: 'lightblue',
    padding: 16,
    paddingHorizontal: 20,
    borderRadius: 12,
  },
  servicesTitle: {
    color: 'white',
    fontSize: 16,
  },
  bodyContainer: {
    marginTop: 24,
    width: '100%',
    alignItems: 'center',
  },
  buttonWrapper: {
    width: '80%',
    // backgroundColor: 'red',
    borderRadius: 30,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  listContainer: {
    marginTop: 36,
    // backgroundColor: 'blue',
    width: '80%',
    // height: 40,
    // paddingVertical: 20,
  },
  listItemContainer: {
    marginVertical: 12,
    borderRadius: 12,
    backgroundColor: '#e4f4f5',
    paddingHorizontal: 12,
    paddingVertical: 6,
    // height: 40,
  },
  listItemWrapper: {
    // backgroundColor: 'yellow',
    // height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listItemQueueNumberTitle: {
    fontSize: 16,
    color: '#000',
  },
  listItemNameTitle: {
    fontSize: 18,
    color: '#000',
  },
  listItemNameWrapper: {
    width: '60%',
  },
  servicesWrapper: {
    flexDirection: 'row',
  },
  servicesItemWrapper: {
    // backgroundColor: 'red',
    paddingHorizontal: 6,
    // marginLeft: 6,
    alignItems: 'center',
  },
  servicesLabel: {
    color: '#000',
  },
  listItemTotalWrapper: {
    // backgroundColor: 'green',
    borderLeftWidth: 0.5,
    width: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  listItemTotalTitle: {
    fontSize: 18,
    color: '#000',
  },
  listItemTotalPriceTitle: {
    fontWeight: '600',
    fontSize: 22,
    color: '#000',
  },
});
