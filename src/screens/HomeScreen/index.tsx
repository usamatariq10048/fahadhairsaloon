import React, {useEffect, useState} from 'react';
import {
  Button,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {styles} from './style';
import SendSMS from 'react-native-sms';

const HomeScreen = () => {
  type servicesDataTypes = {
    name: string;
    price: number;
  };
  type customerDataTypes = {
    queueNo: number;
    Name: string;
    Mobile: string;
    services: Array<servicesDataTypes>;
  };
  const [isValid, setIsValid] = useState(false);
  const [customerName, setCustomerName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [queueNumber, setQueueNumber] = useState(1);
  const [currentCustomersList, setCurrentCustomersList] = useState(
    Array<customerDataTypes>,
  );
  const [waitingCustomersList, setWaitingCustomersList] = useState(
    Array<customerDataTypes>,
  );
  const [doneCustomersList, setDoneCustomerList] = useState(
    Array<customerDataTypes>,
  );

  useEffect(() => {}, []);
  const [isHair, setIsHair] = useState(false);
  const [isBeard, setIsBeard] = useState(false);
  const [isFacial, setIsFacial] = useState(false);

  const [selectedServices, setSelectedServices] = useState<servicesDataTypes[]>(
    [],
  );

  useEffect(() => {
    setIsValid(
      customerName.length > 0 && mobileNumber.length === 11 ? true : false,
    );
  }, [customerName, mobileNumber]);

  const reset = () => {
    setCustomerName('');
    setMobileNumber('');
    setIsBeard(false);
    setIsHair(false);
    setIsFacial(false);
  };

  useEffect(() => {
    // Check if each service is selected and add it to the selectedServices array
    const updatedSelectedServices = [];

    if (isHair) {
      updatedSelectedServices.push({name: 'Hair', price: 800});
    }

    if (isBeard) {
      updatedSelectedServices.push({name: 'Beard', price: 1500});
    }

    if (isFacial) {
      updatedSelectedServices.push({name: 'Facial', price: 2000});
    }

    setSelectedServices(updatedSelectedServices);
  }, [isHair, isBeard, isFacial]);

  const addNewCustomer = () => {
    const obj: customerDataTypes = {
      queueNo: queueNumber,
      Name: customerName,
      Mobile: mobileNumber,
      services: selectedServices,
    };
    if (currentCustomersList.length === 0) {
      setCurrentCustomersList([obj]);
    } else {
      setWaitingCustomersList([...waitingCustomersList, obj]);
    }

    reset();
    setQueueNumber(queueNumber + 1);
  };

  const nextCustomer = () => {
    if (waitingCustomersList.length > 0) {
      const current = waitingCustomersList[0];
      setWaitingCustomersList(prevCustomers => prevCustomers.slice(1));
      setCurrentCustomersList([current]);
      setDoneCustomerList(prevCustomers => [
        ...prevCustomers,
        ...currentCustomersList,
      ]);
    } else {
      setWaitingCustomersList(prevCustomers => prevCustomers.slice(1));
      setCurrentCustomersList([]);
      setDoneCustomerList(prevCustomers => [
        ...prevCustomers,
        ...currentCustomersList,
      ]);
    }
  };

  const sendSms = () => {
    SendSMS.send(
      {
        body: `Your Number is #${queueNumber}. currently Serving #${
          currentCustomersList.length === 1
            ? currentCustomersList[0].queueNo
            : queueNumber
        }. Your Services: ${selectedServices
          .map(service => service.name)
          .join(', ')}. Your Total Amount is: Rs.${selectedServices
          .map(service => service.price)
          .reduce((prev, price) => {
            return prev + price;
          })}`,
        recipients: [mobileNumber],
        //@ts-ignore
        successTypes: ['sent', 'queued'],
        allowAndroidSendWithoutReadPermission: true,
      },
      (completed, cancelled, error) => {
        console.log(
          'SMS Callback: completed: ' +
            completed +
            ' cancelled: ' +
            cancelled +
            ' error: ' +
            error,
        );
      },
    );
  };

  console.log('waiting Customer List', waitingCustomersList);
  console.log('current Customer List', currentCustomersList);
  console.log('done Customer List', doneCustomersList);
  // console.log(
  //   'Total',
  //   currentCustomersList[0] &&
  //     currentCustomersList[0].services
  //       .map(service => service.price)
  //       .reduce((total, price) => total + price),
  // );
  // console.log(
  //   'selected Total',
  //   selectedServices &&
  //     selectedServices
  //       .map(service => service.price)
  //       .reduce((total, price) => total + price),
  // );
  return (
    <>
      <View style={styles.heading}>
        <Text style={styles.title}>Fahad & Asif Hair Saloon</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.header}>
            <TextInput
              onChangeText={text => setCustomerName(text)}
              placeholder="Customer Name"
              style={styles.inputFields}
              value={customerName}
              editable
            />
            <TextInput
              onChangeText={text => setMobileNumber(text)}
              placeholder="Mobile Number"
              style={styles.inputFields}
              value={mobileNumber}
              editable
            />
            <View style={styles.servicesContainer}>
              <TouchableOpacity
                style={[
                  styles.servicesItem,
                  {backgroundColor: isHair ? 'blue' : 'lightblue'},
                ]}
                onPress={() => setIsHair(!isHair)}>
                <Text style={styles.servicesTitle}>Hair</Text>
                <Text style={styles.servicesTitle}>800</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.servicesItem,
                  {backgroundColor: isBeard ? 'blue' : 'lightblue'},
                ]}
                onPress={() => setIsBeard(!isBeard)}>
                <Text style={styles.servicesTitle}>Beard</Text>
                <Text style={styles.servicesTitle}>1500</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.servicesItem,
                  {backgroundColor: isFacial ? 'blue' : 'lightblue'},
                ]}
                onPress={() => setIsFacial(!isFacial)}>
                <Text style={styles.servicesTitle}>Facial</Text>
                <Text style={styles.servicesTitle}>2000</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.bodyContainer}>
            <View style={styles.buttonWrapper}>
              <Button
                title="Allot Number"
                onPress={() => {
                  sendSms();
                  addNewCustomer();
                }}
                disabled={!isValid || selectedServices.length === 0}
              />
              <Button
                title="Next"
                onPress={() => {
                  // sendSms();
                  // addNewCustomer();
                  nextCustomer();
                }}
                // disabled={!isValid}
              />
            </View>
            <View style={styles.listContainer}>
              <Text>Current Customer</Text>
              {currentCustomersList.map(customer => {
                return (
                  <View style={styles.listItemContainer}>
                    <View style={styles.listItemWrapper}>
                      <View style={styles.listItemNameWrapper}>
                        <Text style={styles.listItemQueueNumberTitle}>
                          Number #{customer.queueNo}
                        </Text>
                        <Text style={styles.listItemNameTitle}>
                          {customer.Name}
                        </Text>
                        <View style={styles.servicesWrapper}>
                          {customer.services.map(item => {
                            return (
                              <View style={styles.servicesItemWrapper}>
                                <Text style={styles.servicesLabel}>
                                  *{item.name}
                                </Text>
                                <Text style={styles.servicesLabel}>
                                  {item.price}
                                </Text>
                              </View>
                            );
                          })}
                        </View>
                      </View>
                      <View style={styles.listItemTotalWrapper}>
                        <Text style={styles.listItemTotalTitle}>Total</Text>
                        <Text style={styles.listItemTotalPriceTitle}>
                          {customer.services.reduce((total, service) => {
                            return total + service.price;
                          }, 0)}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
            <View style={styles.listContainer}>
              <Text>Waiting Customers</Text>
              {waitingCustomersList.map(customer => {
                return (
                  <View style={styles.listItemContainer}>
                    <View style={styles.listItemWrapper}>
                      <View style={styles.listItemNameWrapper}>
                        <Text style={styles.listItemQueueNumberTitle}>
                          Number #{customer.queueNo}
                        </Text>
                        <Text style={styles.listItemNameTitle}>
                          {customer.Name}
                        </Text>
                        <View style={styles.servicesWrapper}>
                          {customer.services.map(item => {
                            return (
                              <View style={styles.servicesItemWrapper}>
                                <Text style={styles.servicesLabel}>
                                  *{item.name}
                                </Text>
                                <Text style={styles.servicesLabel}>
                                  {item.price}
                                </Text>
                              </View>
                            );
                          })}
                        </View>
                      </View>
                      <View style={styles.listItemTotalWrapper}>
                        <Text style={styles.listItemTotalTitle}>Total</Text>
                        <Text style={styles.listItemTotalPriceTitle}>
                          {customer.services.reduce((total, service) => {
                            return total + service.price;
                          }, 0)}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          </View>
        </View>
      </ScrollView>
    </>
  );
};

export default React.memo(HomeScreen);
