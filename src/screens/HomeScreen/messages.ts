/*
 * PrintPreviewScreenScreen Messages
 *
 * This contains all the text for the PrintPreviewScreen component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.screens.PrintPreviewScreen';

export default defineMessages({
  screenTitle: {
    id: `${scope}.screenTitle`,
    defaultMessage: 'Report Preview',
  },
  shareButtonLabel: {
    id: `${scope}.shareButtonLabel`,
    defaultMessage: 'SHARE',
  },
});
