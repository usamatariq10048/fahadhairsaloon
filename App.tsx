/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import AppContainer from './containers/App';

function App() {
  return <AppContainer />;
}

export default App;
